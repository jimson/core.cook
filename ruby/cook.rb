require('build')
require('os')

module Cook
    @@binary_dir = nil
    @@cmd = nil
    @@ninja = nil
    @@default = nil

    def self.binary_dir
        unless @@binary_dir
            @@binary_dir = File.join(Build.root(), "core.cook", "binary")
        end
        @@binary_dir
    end

    def self.fixes(cmd)
        case OS.get
        when :windows
            [File.join("windows", "x86"), "#{cmd}.exe"]
        when :linux
            ["linux", "#{cmd}"]
        when :macOS
            ["macos/x64", "#{cmd}"]
        end
    end

    def self.cmd()
        unless @@cmd
            dir, exec = fixes("cook")
            @@cmd = File.join(binary_dir, "latest", dir, exec)
        end
        @@cmd
    end

    def self.ninja()
        unless @@ninja
            dir, exec = fixes("ninja")
            @@ninja = File.join(binary_dir, "ninja", dir, exec)
        end
        @@ninja
    end

    class Instance
        attr_accessor :toolchain
        attr_accessor :options

        def initialize
            @toolchain = "default"
            @options = []
        end

        def output_dir(fn = nil)
            dir = File.join(Build.dir, ([@toolchain] + @options).join('-'))
            FileUtils.mkdir_p(dir) unless File.exists?(dir)
            dir = File.join(dir, fn) if fn
            dir

        end

        def cmd(additional = [])
            cmd = [Cook.cmd()] + ["-o #{output_dir}"] + ["-t #{@toolchain}"] + @options.map {|o| "-T #{o}"} + additional
            cmd.join(" ")
        end

        def build(tgts = nil)
            options = ["-g ninja"]
            if tgts == nil
            elsif tgts.is_a?(String)
                options << tgts
            else
                options += tgts
            end

            # call cook
            Rake::sh cmd(options)

            # call ninja
            Rake::sh "#{Cook.ninja} -f #{output_dir('build.ninja')}"
        end
    end

    def self.default
        unless @@default
            @@default = Instance.new
        end
        @@default
    end
end

