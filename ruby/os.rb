module OS
    @@os = nil
    :windows
    :linux
    :macOS

    def self.get()
        unless @@os
            if RUBY_PLATFORM =~ /win32/
                @@os = :windows
            elsif RUBY_PLATFORM =~ /linux/
                @@os = :linux
            elsif RUBY_PLATFORM =~ /darwin/
                @@os = :macOS
            else
                @@os= :unknown
                raise "Unsupported operating system: #{RUBY_PLATFORM}"
            end
        end
        @@os
    end

    def self.windows()
        get() == :windows
    end
    
    def self.linux()
        get() == :linux
    end

    def self.macOS()
        get() == :macOS
    end
end
