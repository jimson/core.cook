require('build')
module Git

    def self.process_submodules(dir)
        Dir.chdir(dir) do
            fn = ".gitmodules"
            return false unless File.exists?(fn)

            mod_name= nil
            path = nil
            branch = nil

            mod_rgx = /\[submodule\s+\"(.+)\"\]/
            path_rgx = /path\s*=\s*(.*)/
            branch_rgx = /branch\s*=\s*(.*)/

            File.readlines(fn).each do |line|
                res = nil
                if res = mod_rgx.match(line)
                    yield ({module: mod_name, path: path, branch: branch }) if mod_name && path
                    mod_name = res[1]
                    path = nil
                    branch = nil
                elsif res = path_rgx.match(line)
                    path = res[1]
                elsif res = branch_rgx.match(line)
                    branch = res[1]
                end
            end

            yield ({module: mod_name, path: path, branch: branch }) if mod_name && path
        end
    end

    def self.uth_rec(info, level)
        p = info[:path]
        m = info[:module]
        b = info[:branch]


        if Dir[File.join(p, "*")].empty?
            `git submodule --quiet update --init --recursive #{p}`
        end
        Dir.chdir(p) do 
            `git checkout #{b} --quiet` if b
            `git pull --rebase --quiet`
        end
        
        puts " + " + "  "*level + "#{m}:#{b || '<none>'} [#{File.expand_path(p)}]"
        process_submodules(p) { |info| uth_rec(info, level+1) } if b
    end

    def self.uth()
        dir = Build.root()
        process_submodules(dir) { |info| uth_rec(info, 0) }
    end
end
