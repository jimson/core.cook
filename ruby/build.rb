# location of the build directory
module Build
    @@root = nil
    @@dir = nil
    def self.root()
        unless @@root
            @@root = File.dirname(File.dirname(File.dirname(__FILE__)))
        end
        @@root
    end

    def self.dir(fn = nil)
        unless @@dir
            @@dir = File.join(root, "build")
            FileUtils.mkdir_p(@@dir) unless File.exists?(@@dir)
        end
        dir = @@dir
        dir = File.join(dir, fn) if fn
        dir
    end
end
